<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SendLog extends Model
{
    use HasFactory;

    const ID = 'id';
    const CHAT_ID = 'chat_id';
    const MESSAGE_ID = 'message_id';

    protected $fillable = [
        self::ID,
        self::CHAT_ID,
        self::MESSAGE_ID,
    ];
}
