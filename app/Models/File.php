<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $fillable = ['token_id','caption','video_id','type','uniqe_id'];

    public function token(){

        return $this->belongsTo(Token::class);
    }
}
