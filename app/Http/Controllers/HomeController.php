<?php

namespace App\Http\Controllers;

use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tokens = Token::orderBy('created_at', 'desc')
            ->get();
        $removeConf = Cache::get('remove-conf') ?? false;
        $i = 1;
        return view('home', compact('tokens', 'removeConf', 'i'));
    }

    public function editRemoveConf()
    {
        $removeConf = Cache::get('remove-conf') ?? false;

        Cache::forget('remove-conf');

        Cache::rememberForever('remove-conf', function () use ($removeConf) {
            return !$removeConf;
        });

        return back();
    }
}
