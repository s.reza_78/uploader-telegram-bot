<?php

namespace App\Http\Controllers;

use App\Jobs\RemoveMessagesJob;
use App\Models\File;
use App\Models\SendLog;
use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;

class WebhookController extends Controller
{

    private $removeConf;

    public function __construct()
    {
        $this->removeConf = Cache::get('remove-conf') ?? false;

    }

    public function main()
    {
        // return 200;

        try {

            $telegram = new Api('2026257510:AAEREJeZ8pwDDiovWGb6WUB6nucbFlXBBqo');

            $updates = Telegram::getWebhookUpdates();
            $updates = $updates->all();
            $admin_id = "350297932";

            $chat_id = $updates['message']['chat']['id'];
            $message_id = $updates['message']['message_id'];
            $user_id = $updates['message']['from']['id'];
            // dump($updates['message']['text']);


            $text = array_key_exists('text', $updates['message']);
//            dump($user_id);
            if ($text) {

                $message = $updates['message']['text'];
                $text = explode(" ", $message);
                $join = array('chat_id' => '@imdb1ir_real1s', 'user_id' => $user_id);
                $join_info = $telegram->getChatMember($join);


//                dump($join_info['status']);

                if ($join_info['status'] == "left") {
                    if (count($text) > 1) {
                        $upload = true;
                    } else {
                        $upload = false;
                    }
                    $this->send_inline_join_button($telegram, $text, $chat_id, $upload);
                    return 200;
                }
            }


            /* save files in db */
            $video = array_key_exists('video', $updates['message']);
            $photo = array_key_exists('photo', $updates['message']);
            $text = array_key_exists('text', $updates['message']);
            $document = array_key_exists('document', $updates['message']);


            if ($text) {

                $message = $updates['message']['text'];
                $text = explode(" ", $message);
                $text_num = count($text);

                if ($message == '/command1') {
                    if ($user_id != $admin_id && $user_id != 817787314 && $user_id != 533997436) {

                        return 200;
                    }
                    $this->set_link($chat_id, $message_id, $telegram);
                } else if ($message == '/command2') {

                    if ($user_id != $admin_id && $user_id != 817787314 && $user_id != 533997436) {
                        return 200;
                    }

                    $this->show_uploads($chat_id, $message_id, $telegram);
                } else if ($text[0] == '/start' && $text_num > 1) {

                    $token = $text[1];
                    if ($token) {
                        $this->send_content($token, $chat_id, $message_id, $telegram);
                    }
                } else if ($text[0] == 'delete' && $text_num > 1) {

                    $token = $text[1];
                    if ($token) {
                        $this->delete_file($telegram, $message_id, $chat_id, $token);
                    }
                }
                return 200;
            } else if ($video) {
                if ($user_id != $admin_id && $user_id != 817787314 && $user_id != 533997436) {

                    return 200;
                }
                $type = "video";
                $uniqe_id = $updates['message'][$type]['file_unique_id'];
                $movie_id = $updates['message'][$type]['file_id'];

                $caption = $updates['message']['caption'];
                $this->save_files($movie_id, $caption, $type, $chat_id, $telegram, $uniqe_id, $message_id);
            } else if ($photo) {
                if ($user_id != $admin_id && $user_id != 817787314 && $user_id != 533997436) {

                    return 200;
                }
                $type = "photo";
                $uniqe_id = $updates['message'][$type][0]['file_unique_id'];
                $movie_id = $updates['message'][$type][0]['file_id'];

                $caption = $updates['message']['caption'];
                $this->save_files($movie_id, $caption, $type, $chat_id, $telegram, $uniqe_id, $message_id);
            } else if ($document) {
                if ($user_id != $admin_id && $user_id != 817787314 && $user_id != 533997436) {

                    return 200;
                }
                $type = "document";
                $uniqe_id = $updates['message'][$type]['file_unique_id'];
                $movie_id = $updates['message'][$type]['file_id'];

                $caption = $updates['message']['caption'];
                $this->save_files($movie_id, $caption, $type, $chat_id, $telegram, $uniqe_id, $message_id);
            }


            return 200;
        } catch (\Throwable $th) {

            dump($th->getMessage());
            return 200;
        }
    }


    public function save_files($movie_id, $caption, $type, $chat_id, $telegram, $uniqe_id, $message_id)
    {


        $file = File::where('uniqe_id', $uniqe_id)->first();
        if ($file) {
            $response = $telegram->sendMessage([
                'chat_id'             => $chat_id,
                'text'                => 'فایل تکراری است',
                'reply_to_message_id' => $message_id,

            ]);
        } else {


            $file = File::Create(['video_id' => $movie_id, 'caption' => $caption, 'type' => $type, 'uniqe_id' => $uniqe_id]);
            if ($file) {
                $response = $telegram->sendMessage([
                    'chat_id'             => $chat_id,
                    'text'                => 'فایل  مورد نظر سیو شد',
                    'reply_to_message_id' => $message_id,

                ]);
            }
        }
    }


    public function send_content($token, $chat_id, $message_id, $telegram)
    {
        $token = Token::where('token', $token)->first();
        $files = $token->files;
        $token->update(['count' => $token->count + 1]);
        if (count($files) > 0) {
            foreach ($files as $file) {


                if ($file->type == 'photo') {
                    $response = $telegram->sendPhoto([
                        'chat_id'             => $chat_id,
                        'photo'               => $file->video_id,
                        'caption'             => $file->caption,
                        'reply_to_message_id' => $message_id
                    ]);
                    dump($response);
                } else if ($file->type == 'document') {
                    $response = $telegram->sendDocument([
                        'chat_id'             => $chat_id,
                        'document'            => $file->video_id,
                        'caption'             => $file->caption,
                        'reply_to_message_id' => $message_id
                    ]);
                    dump($response);

                } else {
                    $response = $telegram->sendVideo([
                        'chat_id'             => $chat_id,
                        'video'               => $file->video_id,
                        'caption'             => $file->caption,
                        'reply_to_message_id' => $message_id
                    ]);
                    dump($response);

                }

                if ($this->removeConf && $response['message_id'] ?? null) {

                    SendLog::create([
                        SendLog::CHAT_ID    => $chat_id,
                        SendLog::MESSAGE_ID => $response['message_id'],
                    ]);

                }
            }

            if ($this->removeConf) {
                $response = $telegram->sendMessage([
                     'chat_id' => $chat_id,
                    'text'    => '☑️ کاربر گرامی ❤️‍🔥
فایل های بالا تا چند دقیقه دیگر از ربات پاک می‌شوند❌
📌برای دانلود، ابتدا فیلم را به save message خود ارسال کنید سپس  از آنجا دانلود کنید✅
به هیچ عنوان از داخل ربات دانلود نکنید❌
سپاس از همراهی و حمایت شما 💐💐 '
                ]);

                if ($response['message_id'] ?? null) {

                    SendLog::create([
                        SendLog::CHAT_ID    => $chat_id,
                        SendLog::MESSAGE_ID => $response['message_id'],
                    ]);

                }
            }
        }
    }


    public function set_link($chat_id, $message_id, $telegram)
    {


        $files = File::where('token_id', null)->get();
        if (count($files) > 0) {
            $token = Token::create(['token' => $this->generateToken()]);

            foreach ($files as $file) {

                $file->update(['token_id' => $token->id]);
            }
        } else {
            $response = $telegram->sendMessage([
                'chat_id'             => $chat_id,
                'text'                => 'همه ی آپلود ها دارای لینک می باشند',
                'reply_to_message_id' => $message_id,

            ]);
        }
        if ($token) {
            $response = $telegram->sendMessage([
                'chat_id'             => $chat_id,
                'text'                => 'http://t.me/imdbUpload_bot?start=' . $token->token,
                'reply_to_message_id' => $message_id,


            ]);
        } else {
            $response = $telegram->sendMessage([
                'chat_id'             => $chat_id,
                'text'                => 'فایلی وجود ندارد ',
                'reply_to_message_id' => $message_id,


            ]);
        }
    }


    function generateToken()
    {
        return md5(rand(1, 10) . microtime());
    }


    public function show_uploads($chat_id, $message_id, $telegram)
    {


        $tokens = Token::all();
        if (count($tokens) == 0) {
            $response = $telegram->sendMessage([
                'chat_id'             => $chat_id,
                'text'                => 'آپلودی نکرده اید',
                'reply_to_message_id' => $message_id,

            ]);
            return 200;
        }
        $tokens = Token::orderBy('created_at', 'desc')->get();
        $text = "";
        foreach ($tokens as $token) {
            foreach ($token->files as $file) {


                if ($file->type == 'photo') {
                    $response = $telegram->sendPhoto([
                        'chat_id'             => $chat_id,
                        'photo'               => $file->video_id,
                        'caption'             => $file->caption . "\n" . 'delete = ' . 'delete ' . $file->uniqe_id . "\nlink =" . "http://t.me/imdbUpload_bot?start=" . $token->token,
                        'reply_to_message_id' => $message_id
                    ]);
                } elseif ($file->type == 'document') {

                    $response = $telegram->sendDocument([
                        'chat_id'             => $chat_id,
                        'document'            => $file->video_id,
                        'caption'             => $file->caption,
                        'reply_to_message_id' => $message_id
                    ]);
                } else {
                    $response = $telegram->sendVideo([
                        'chat_id'             => $chat_id,
                        'video'               => $file->video_id,
                        'caption'             => $file->caption . "\n" . 'delete = ' . 'delete ' . $file->uniqe_id . "\nlink =" . "http://t.me/imdbUpload_bot?start=" . $token->token,
                        'reply_to_message_id' => $message_id
                    ]);
                }

                break;

            }
        }


    }


    public function send_inline_join_button($telegram, $text, $chat_id, $upload)
    {
        if ($upload) {
            $inline_keyboard = json_encode([ //Because its object
                                             'inline_keyboard' => [
                                                 [
                                                     ['text' => 'عضو شدن در کانال', 'url' => "https://t.me/Imdb1ir_real1s"],
                                                     ['text' => 'عضو شدم', 'url' => "http://t.me/imdbUpload_bot?start=" . $text[1]]
                                                 ],
                                             ]
            ]);
        } else {

            $inline_keyboard = json_encode([ //Because its object
                                             'inline_keyboard' => [
                                                 [
                                                     ['text' => 'عضو شدن در کانال', 'url' => "https://t.me/Imdb1ir_real1s"],
                                                     ['text' => 'عضو شدم', 'url' => "http://t.me/imdbUpload_bot?start"]
                                                 ],
                                             ]
            ]);
        }
        $response = $telegram->sendMessage([
            'chat_id'      => $chat_id,
            'text'         => 'با سلام شما در کانال عضو نیستید ابتدا عضو کانال شوید بعد روی گزینه عضو شدم کلیک کنید',
            'reply_markup' => $inline_keyboard
        ]);
    }


    public function delete_file($telegram, $message_id, $chat_id, $token)
    {


        $file = File::where('uniqe_id', $token)->first();
        if ($file) {
            $file->delete();

            $response = $telegram->sendMessage([
                'chat_id'             => $chat_id,
                'text'                => 'فایل مورد نظر پاک شد',
                'reply_to_message_id' => $message_id

            ]);
        } else {
            $response = $telegram->sendMessage([
                'chat_id'             => $chat_id,
                'text'                => 'فایل مورد نظر وجود ندارد ',
                'reply_to_message_id' => $message_id

            ]);
        }
    }
}



// dump($chat_id);
//         // return 200;

//         // dump($updates);
//     // return 200;
//         // $text = $updates->all()['message']['text'];
//         // $text = explode(" ", $text);

//         // dump($text);
//         // dump($updates->all()['message']['video']);
//         // AgADIwsAAjr6kVA
//         // $response = $telegram->getFile(['file_id' => $text[1]]);

//         // dump($response);
//         $response = $telegram->getFile(['file_id' => 'BAACAgQAAxkBAAMwYWL6zs1b8JKS3uvyyXGZQtH_KUIAAq0KAALNCiBRRNgyfkc7roUhBA']);
//        $path =  $response['file_path'];
//        dump($path);
//     // return 200;
//         // AAMCBAADGQEAAyBhYvgsfyuvigkxtnfHVAJFcqEgqgACrQoAAs0KIFFE2DJ-RzuuhQEAB20AAyEE
//         $response = $telegram->sendVideo([
//                 'chat_id' => $chat_id,
//                 'video' => 'BAACAgQAAxkBAAMwYWL6zs1b8JKS3uvyyXGZQtH_KUIAAq0KAALNCiBRRNgyfkc7roUhBA',
//                 'caption'=>"hoy"
//               ]);
