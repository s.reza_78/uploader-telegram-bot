<?php

namespace App\Console\Commands;

use App\Models\SendLog;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class DeleteMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $logs = SendLog::where(SendLog::CREATED_AT, '<=', now()->subSeconds(60))
            ->get();
        $telegram = new Api('2026257510:AAEREJeZ8pwDDiovWGb6WUB6nucbFlXBBqo');


        foreach ($logs as $log) {

            try {
                $telegram->deleteMessage([
                    'chat_id'    => $log->chat_id,
                    'message_id' => $log->message_id
                ]);

                $log->delete();
            } catch (\Exception $e) {

            }
        }
    }
}
