<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;

class RemoveMessagesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $chat_id;
    private $message_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($chat_id, $message_id)
    {
        $this->chat_id = $chat_id;
        $this->message_id = $message_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $telegram = new Api('2026257510:AAEREJeZ8pwDDiovWGb6WUB6nucbFlXBBqo');

        $telegram->deleteMessage([
            'chat_id'    => $this->chat_id,
            'message_id' => $this->message_id
        ]);
    }

}
