<?php

use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Route;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {



    return view('layouts.app');
});



Route::get('set-webhook', function () {

    $telegram = new Api('2026257510:AAEREJeZ8pwDDiovWGb6WUB6nucbFlXBBqo');

    $response = $telegram->setWebhook(['url' => 'https://imdbserial.xyz/bot2026257510:AAEREJeZ8pwDDiovWGb6WUB6nucbFlXBBqo/webhook']);
    return $response;
});


Route::post('/bot2026257510:AAEREJeZ8pwDDiovWGb6WUB6nucbFlXBBqo/webhook', [App\Http\Controllers\WebhookController::class, 'main']);




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/edit/remove-config', [App\Http\Controllers\HomeController::class, 'editRemoveConf'])->name('edit.remove.conf');


Route::resource('/token', App\Http\Controllers\TokenController::class);
