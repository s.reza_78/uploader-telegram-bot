@extends('layouts.app')

@section('content')

<table id="table_id" class="table table-striped table-bordered"  class="display">
    <thead>
        <tr>
            <th>token_id</th>
            <th>caption</th>
            <th>type</th>
            <th>downloaded</th>


            <th>delete</th>
        </tr>
    </thead>
    <tbody>
        
        @foreach ($tokens as $token )
        @foreach ($token->files as $file )
        <tr>
            <td>{{$token->id}}</td>
            <td>{{$file->caption}}</td>
            <td>{{$file->type}}</td>
            <td>{{$token->count}}</td>


            <td>
                <form action="{{route('token.destroy',$file->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value=" delete "class="btn btn-danger">
                </form></td>

        </tr>
        @endforeach
        @endforeach

    </tbody>
</table>
@endsection
